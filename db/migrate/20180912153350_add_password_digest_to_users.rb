class AddPasswordDigestToUsers < ActiveRecord::Migration[5.2]
  def new
    add_column :users, :password_digest, :string
  end
end
