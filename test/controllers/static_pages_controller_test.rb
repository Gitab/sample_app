require 'test_helper'
include ApplicationHelper
class StaticPagesControllerTest < ActionDispatch::IntegrationTest
 test "should get root" do
 get root_path
 assert_response :success
 assert_select "title", "LEARNER"
 end
 
 test "should get home" do
    get root_path
    assert_response :success
	assert_select "title", "LEARNER" #assert_select use to give same name for 3 file
end

  test "should get help" do
        get help_path

    assert_response :success
	assert_select "title", "LEARNER"
  end

    test "should get about" do
    get about_path
    assert_response :success
	assert_select "title", "LEARNER"
  end
  
  test "should get contact" do
      get contact_path
    assert_response :success
  assert_select "title", "LEARNER"
  end
 
 
end
